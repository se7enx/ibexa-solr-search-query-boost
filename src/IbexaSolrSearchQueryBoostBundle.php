<?php

declare(strict_types=1);

namespace ContextualCode\IbexaSolrSearchQueryBoost;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IbexaSolrSearchQueryBoostBundle extends Bundle
{
}