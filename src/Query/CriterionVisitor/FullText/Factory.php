<?php

declare(strict_types=1);

namespace ContextualCode\IbexaSolrSearchQueryBoost\Query\CriterionVisitor\FullText;

use eZ\Publish\Core\Search\Common\FieldNameResolver;
use EzSystems\EzPlatformSolrSearchEngine\FieldMapper\IndexingDepthProvider;
use EzSystems\EzPlatformSolrSearchEngine\Query\Common\CriterionVisitor\Factory\FullTextFactoryAbstract;
use EzSystems\EzPlatformSolrSearchEngine\Query\CriterionVisitor;
use QueryTranslator\Languages\Galach\Generators\ExtendedDisMax;
use QueryTranslator\Languages\Galach\Parser;
use QueryTranslator\Languages\Galach\Tokenizer;

/**
 * Factory for FullText Criterion Visitor.
 *
 * @see \EzSystems\EzPlatformSolrSearchEngine\Query\Content\CriterionVisitor\FullText
 *
 * @internal
 */
final class Factory extends FullTextFactoryAbstract
{
    /**
     * @var BoostQueryParser
     */
    private $boostQueryParser;

    public function __construct(
        FieldNameResolver $fieldNameResolver,
        Tokenizer $tokenizer,
        Parser $parser,
        ExtendedDisMax $generator,
        IndexingDepthProvider $indexingDepthProvider,
        BoostQueryParser $boostQueryParser
    ) {
        parent::__construct(
            $fieldNameResolver,
            $tokenizer,
            $parser,
            $generator,
            $indexingDepthProvider
        );

        $this->boostQueryParser = $boostQueryParser;
    }

    /**
     * Create FullText Criterion Visitor.
     *
     * @return CriterionVisitor|FullText
     */
    public function createCriterionVisitor(): CriterionVisitor
    {
        return new FullText(
            $this->fieldNameResolver,
            $this->tokenizer,
            $this->parser,
            $this->generator,
            $this->boostQueryParser,
            $this->indexingDepthProvider->getMaxDepth()
        );
    }
}
