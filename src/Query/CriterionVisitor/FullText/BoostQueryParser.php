<?php

declare(strict_types=1);

namespace ContextualCode\IbexaSolrSearchQueryBoost\Query\CriterionVisitor\FullText;

use ContextualCode\IbexaSolrSearchQueryBoost\Query\Criterion\FullText as FullTextCriterion;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Values\ContentType\ContentType;
use eZ\Publish\API\Repository\Values\ContentType\FieldDefinition;
use eZ\Publish\Core\Search\Common\FieldNameResolver;

class BoostQueryParser
{
    private const CONTENT_TYPE_FIELD = 'content_type_id_id';

    /**
     * @var ContentTypeService
     */
    private $contentTypeService;

    /**
     * @var FieldNameResolver
     */
    protected $fieldNameResolver;


    public function __construct(
        ContentTypeService $contentTypeService,
        FieldNameResolver $fieldNameResolver
    ) {
        $this->contentTypeService = $contentTypeService;
        $this->fieldNameResolver = $fieldNameResolver;
    }

    public function parse(FullTextCriterion $criterion): ?string
    {
        $boostQueryParts = [];

        $boostQueryParts = array_merge($boostQueryParts, $this->parseFields($criterion));
        $boostQueryParts = array_merge($boostQueryParts, $this->parseTypes($criterion));

        return count($boostQueryParts) ? implode(' ', $boostQueryParts) : null;
    }

    private function parseFields(FullTextCriterion $criterion): array
    {
        $boostQueryParts = [];

        if (!isset($criterion->queryBoost['fields'])) {
            return $boostQueryParts;
        }

        // We need to wrap the search query with * in order to make it working for string values
        // Otherwise it will work just for text
        $query = '*' . $criterion->value . '*';

        foreach ($criterion->queryBoost['fields'] as $typeIdentifier => $fields) {
            $type = $this->contentTypeService->loadContentTypeByIdentifier($typeIdentifier);

            foreach ($fields as $fieldIdentifier => $boost) {
                $fieldDefinition = $this->getContentTypeField($type, $fieldIdentifier);
                if ($fieldDefinition === null) {
                    continue;
                }

                $fieldName = $this->fieldNameResolver->getIndexFieldName(
                    $criterion,
                    $type->identifier,
                    $fieldDefinition->identifier,
                    $fieldDefinition->fieldTypeIdentifier,
                    null,
                    false
                );

                $boostQueryParts[] = array_keys($fieldName)[0] . ':' . $query . '^' . $boost;
            }
        }

        return $boostQueryParts;
    }

    private function parseTypes(FullTextCriterion $criterion): array
    {
        $boostQueryParts = [];

        if (!isset($criterion->queryBoost['types'])) {
            return $boostQueryParts;
        }

        foreach ($criterion->queryBoost['types'] as $identifier => $boost) {
            $type = $this->contentTypeService->loadContentTypeByIdentifier($identifier);

            $boostQueryParts[] = self::CONTENT_TYPE_FIELD . ':' . $type->id . '^' . $boost;
        }

        return $boostQueryParts;
    }

    private function getContentTypeField(ContentType $type, string $identifier): ?FieldDefinition
    {
        foreach ($type->fieldDefinitions as $fieldDefinition) {
            if ($fieldDefinition->identifier === $identifier) {
                return $fieldDefinition;
            }
        }

        return null;
    }
}