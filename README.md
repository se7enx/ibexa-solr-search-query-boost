# Ibexa Solr Search Query Boost

This package implements query boosting for the Solr search query in Ibexa DXP.

## Installation

Require via composer:

 ```bash
 composer require contextualcode/ibexa-solr-search-query-boost
 ```

## Usage

1. You need to use `ContextualCode\IbexaSolrSearchQueryBoost\Query\Criterion\FullText` instead of `eZ\Publish\API\Repository\Values\Content\Query\Criterion\FullText`:
    ```php
    <?php 
    ...
    - use eZ\Publish\API\Repository\Values\Content\Query\Criterion\FullText;
    + use ContextualCode\IbexaSolrSearchQueryBoost\Query\Criterion\FullText;
    ...
    ```

2. New `queryBoost` parameter is available for the updated `FullText` criteria. It expects to have `fields` and/or `types` parameters:
    ```php
    <?php 
    ...
    - $fullTextQuery = new FullText('test');
    + $fullTextQuery = new FullText('test', ['queryBoost' => $queryBoost]);
    ...
    ```

The example `src/Controller/CustomSearch.php` controller with implemented query boost:
```php
<?php

namespace App\Controller;

use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ContextualCode\IbexaSolrSearchQueryBoost\Query\Criterion\FullText;

class CustomSearch extends AbstractController
{
    private $searchService;

    public function __construct(SearchService $searchService) {
        $this->searchService = $searchService;
    }

    public function search(Request $request): JsonResponse
    {
        $queryBoost = [
            'fields' => [
                'article' => ['title' => 5, 'intro' => 2],
                'folder' => ['name' => 3],
            ],
            'types' => ['article' => 10, 'folder' => 0.8],
        ];
        $query = new Query([
            'query' => new FullText('test', ['queryBoost' => $queryBoost])
        ]);
        $results = $this->searchService->findContent($query);

        return new JsonResponse($results);
    }
}
```
